class Item extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      editable: false,
      itemName: '',
      itemDescription: ''
    }
    this.handleEdit = this.handleEdit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleEdit() {
    if(this.state.editable) {
      var name = this.state.itemName;
      var id = this.props.item.id;
      var description = this.state.itemDescription;
      var item = {id: id , name: name , description: description};
      this.props.handleUpdate(item);
    }
    this.setState({ editable: !this.state.editable })
  }

  render() {
    var name = this.state.editable ?
      <input type='text' name='itemName' defaultValue={this.props.item.name} onChange={this.handleInputChange}/> :
      <h3>{this.props.item.name}</h3>
    var description = this.state.editable ?
      <input type='text' name='itemDescription' defaultValue={this.props.item.description} onChange={this.handleInputChange}/> :
      <p>{this.props.item.description}</p>
    return(
      <div>
        {name}
        {description}
        <button onClick={this.handleEdit}>{this.state.editable ? 'SUBMIT' : 'EDIT'}</button>
        <button onClick={this.props.handleDelete}>DELETE</button>
      </div>
    )
  }
}
