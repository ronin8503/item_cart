class NewItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemName: '',
      itemDescription: ''
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleClick() {
    var name = this.state.itemName;
    var description = this.state.itemDescription;
    //console.log('The name value is ' + name + ' the description value is ' + description);
    $.ajax({
      url: '/api/v1/items',
      type: 'POST',
      data: { item: { name: name, description: description } },
      success: (item) => {
        this.props.handleSubmit(item);
      }
    });
  }

  render() {
     return (
      <div>
        <h2>Add new item</h2>
        <input
          type='text'
          placeholder='Name'
          name='itemName'
          value={this.state.itemName}
          onChange={this.handleInputChange}
          />
        <input
          type='text'
          placeholder='Description'
          name='itemDescription'
          value={this.state.itemDescription}
          onChange={this.handleInputChange}
          />
        <button onClick={this.handleClick}>ADD</button>
      </div>
     )
   }
}
